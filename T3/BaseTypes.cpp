#include <iostream>
#include <algorithm>
#include <iterator>
#include <limits>
#include <mutex>
#include <PointKeys.h>

#include "BaseTypes.h"

namespace moroz
{
  std::istream& operator>> (std::istream& in, Point& dest)
  {
    std::istream::sentry sentry(in);

    if (!sentry)
    {
      return in;
    }

    Point temp;

    in >> DelimeterIO{ '(' } >> temp.x >> DelimeterIO{ ';' } >> temp.y >> DelimeterIO{ ')' };

    if (in)
    {
      dest = temp;
    }

    return in;
  }

  std::istream& operator>> (std::istream& in, Polygon& dest)
  {
    std::istream::sentry sentry(in);

    if (!sentry)
    {
      return in;
    }

    int amountOfPoints = 0;
    Polygon temp;

    in >> amountOfPoints;

    if (in && (amountOfPoints > 2))
    {
      std::mutex mutex;

      mutex.lock();

      std::copy_if
      (
        std::istream_iterator< Point >(in),
        std::istream_iterator< Point >(),
        std::back_inserter(temp.points),
        [&in](const Point&)
        {
          if (in.peek() == '\n')
          {
            in.setstate(std::ios_base::eofbit);
          }
          return true;
        }
      );

      mutex.unlock();

      if (amountOfPoints == static_cast< int >(temp.points.size()))
      {
        dest = temp;
        in.clear();
      }
      else
      {
        in.clear();
        in.ignore(std::numeric_limits< std::streamsize >::max(), '\n');
        in.setstate(std::ios::failbit);
      }
    }
    else
    {
      in.clear();
      in.ignore(std::numeric_limits< std::streamsize >::max(), '\n');
      in.setstate(std::ios::failbit);
    }

    return in;
  }
}
