#ifndef BASE_TYPES
#define BASE_TYPES

#include <iosfwd>
#include <vector>

namespace moroz
{
  struct Point
  {
    int x;
    int y;
  };

  struct Polygon
  {
    std::vector< Point > points;
  };

  std::istream& operator>> (std::istream& in, Point& dest);
  std::istream& operator>> (std::istream& in, Polygon& dest);
}

#endif
