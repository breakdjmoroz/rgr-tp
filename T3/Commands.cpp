#include <algorithm>
#include <functional>
#include <numeric>

#include "Commands.h"

namespace moroz
{
  double ParallelogramAreaGetter::operator()(const double acc, const Point& a, const Point& c)
  {
    std::pair< int, int > vectorA{ b_.x - a.x, b_.y - a.y };
    std::pair< int, int > vectorB{ c.x - a.x, c.y - a.y };

    b_.x = c.x;
    b_.y = c.y;

    return acc + std::abs((vectorA.first * vectorB.second) - (vectorA.second * vectorB.first));
  }

  double getFigureArea(const Polygon& figure)
  {
    using namespace std::placeholders;

    ParallelogramAreaGetter getAreaFunctor(figure.points[1]);

    double area = std::accumulate
    (
      figure.points.cbegin() + 2,
      figure.points.cend(),
      0.0,
      std::bind(getAreaFunctor, _1, figure.points[0], _2)
    );

    return area / 2.0;
  }

  double odd(double acc, const Polygon& figure)
  {
    return (figure.points.size() % 2) ? acc + getFigureArea(figure) : acc;
  }

  double even(double acc, const Polygon& figure)
  {
    return (figure.points.size() % 2) ? acc : acc + getFigureArea(figure);
  }

  bool odd(const Polygon& figure)
  {
    return (figure.points.size() % 2) ? true : false;
  }

  bool even(const Polygon& figure)
  {
    return (figure.points.size() % 2) ? false : true;
  }

  double getArea(const std::vector< Polygon >& figures, double (*functor)(double, const Polygon&))
  {
    return std::accumulate(figures.begin(), figures.end(), 0.0, functor);
  }

  double getAreaMean(const std::vector< Polygon >& figures)
  {
    double size = static_cast< double >(figures.size());
    return std::accumulate
    (
      figures.begin(),
      figures.end(),
      0.0,
      [](double acc, const Polygon& figure)
      { return acc + getFigureArea(figure); }
    ) / size;
  }

  double getAreaOfPoints(const std::vector< Polygon >& figures, int number)
  {
    return std::accumulate
    (
      figures.begin(),
      figures.end(),
      0.0,
      [number](double acc, const Polygon& figure)
      { return (static_cast< int >(figure.points.size()) == number) ? acc + getFigureArea(figure) : acc; }
    );
  }

  bool areaComparator(const Polygon& figure1, const Polygon& figure2)
  {
    return getFigureArea(figure1) < getFigureArea(figure2);
  }

  bool vertexComparator(const Polygon& figure1, const Polygon& figure2)
  {
    return figure1.points.size() < figure2.points.size();
  }

  double maxArea(const std::vector< Polygon >& figures)
  {
    return getFigureArea(*std::max_element(figures.begin(), figures.end(), areaComparator));
  }

  int maxVertex(const std::vector< Polygon >& figures)
  {
    return std::max_element(figures.begin(), figures.end(), vertexComparator)->points.size();
  }

  double minArea(const std::vector< Polygon >& figures)
  {
    return getFigureArea(*std::min_element(figures.begin(), figures.end(), areaComparator));
  }

  int minVertex(const std::vector< Polygon >& figures)
  {
    return std::min_element(figures.begin(), figures.end(), vertexComparator)->points.size();
  }

  int count(const std::vector< Polygon >& figures, bool (*functor)(const Polygon&))
  {
    return std::count_if(figures.begin(), figures.end(), functor);
  }

  int count(const std::vector< Polygon >& figures, int numberOfVertex)
  {
    return std::count_if
    (
      figures.begin(),
      figures.end(),
      [numberOfVertex](const Polygon& figure) { return numberOfVertex == static_cast< int >(figure.points.size()); }
    );
  }

  bool xPointComparator(const Point& point1, const Point& point2)
  {
    return point1.x < point2.x;
  }

  bool yPointComparator(const Point& point1, const Point& point2)
  {
    return point1.y < point2.y;
  }

  bool xComparator(const Polygon& figure1, const Polygon& figure2)
  {
    bool result = false;

    int firstMax = std::max_element(figure1.points.begin(), figure1.points.end(), xPointComparator)->x;
    int secondMax = std::max_element(figure2.points.begin(), figure2.points.end(), xPointComparator)->x;

    int firstMin = std::min_element(figure1.points.begin(), figure1.points.end(), xPointComparator)->x;
    int secondMin = std::min_element(figure2.points.begin(), figure2.points.end(), xPointComparator)->x;

    if ((firstMax < secondMax) && (firstMin < secondMin))
    {
      result = true;
    }

    return result;
  }

  bool yComparator(const Polygon& figure1, const Polygon& figure2)
  {
    bool result = false;

    int firstMax = std::max_element(figure1.points.begin(), figure1.points.end(), yPointComparator)->y;
    int secondMax = std::max_element(figure2.points.begin(), figure2.points.end(), yPointComparator)->y;

    int firstMin = std::min_element(figure1.points.begin(), figure1.points.end(), yPointComparator)->y;
    int secondMin = std::min_element(figure2.points.begin(), figure2.points.end(), yPointComparator)->y;

    if ((firstMax < secondMax) && (firstMin < secondMin))
    {
      result = true;
    }

    return result;
  }

  bool inFrame(const Polygon& figure, const std::vector< Polygon >& figures)
  {
    Polygon rectMaxX = *std::max_element(figures.begin(), figures.end(), xComparator);
    Polygon rectMinX = *std::min_element(figures.begin(), figures.end(), xComparator);
    Polygon rectMaxY = *std::max_element(figures.begin(), figures.end(), yComparator);
    Polygon rectMinY = *std::min_element(figures.begin(), figures.end(), yComparator);

    std::pair< int, int > downLeftCorner
    { std::min_element(rectMinX.points.begin(), rectMinX.points.end(), xPointComparator)->x,
      std::min_element(rectMinY.points.begin(), rectMinY.points.end(), yPointComparator)->y };

    std::pair< int, int > upRightCorner
    { std::max_element(rectMaxX.points.begin(), rectMaxX.points.end(), xPointComparator)->x,
      std::max_element(rectMaxY.points.begin(), rectMaxY.points.end(), yPointComparator)->y };

    int maxX = std::max_element(figure.points.begin(), figure.points.end(), xPointComparator)->x;
    int minX = std::min_element(figure.points.begin(), figure.points.end(), xPointComparator)->x;
    int maxY = std::max_element(figure.points.begin(), figure.points.end(), yPointComparator)->y;
    int minY = std::min_element(figure.points.begin(), figure.points.end(), yPointComparator)->y;

    return (maxX <= upRightCorner.first) && (maxY <= upRightCorner.second)
      && (minX >= downLeftCorner.first) && (minY >= downLeftCorner.second);
  }

  bool fullPointComparator(const Point& point1, const Point& point2)
  {
    bool result = false;

    if (point1.x < point2.x)
    {
      result = true;
    }
    else if (point1.x == point2.x)
    {
      result = point1.y < point2.y;
    }

    return result;
  }

  bool EqualFunctor::operator()(const Point& point1, const Point& point2)
  {
    return ((point2.x - point1.x) == coordinate_.first) && ((point2.y - point1.y) == coordinate_.second);
  }

  bool isSame(const Polygon& figure1, const Polygon& figure2)
  {
    bool result = figure1.points.size() == figure2.points.size();

    if (result)
    {
      Polygon temp = figure2;

      std::sort(temp.points.begin(), temp.points.end(), fullPointComparator);

      EqualFunctor equal(figure1.points[0], temp.points[0]);

      result = std::equal
      (
        figure1.points.begin() + 1,
        figure1.points.end(),
        temp.points.begin() + 1, temp.points.end(), equal
      );
    }

    return result;
  }

  int same(const Polygon& figure, const std::vector< Polygon >& figures)
  {
    using namespace std::placeholders;

    Polygon temp = figure;

    std::sort(temp.points.begin(), temp.points.end(), fullPointComparator);

    int counter = std::count_if(figures.begin(), figures.end(), std::bind(isSame, temp, _1));

    return counter;
  }
}
