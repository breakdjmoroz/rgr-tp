#ifndef COMMANDS
#define COMMANDS

#include <vector>

#include "BaseTypes.h"

namespace moroz
{
  class ParallelogramAreaGetter
  {
  public:

    ParallelogramAreaGetter(const Point& b):
      b_(b)
    {}

    double operator()(const double acc, const Point& a, const Point& c);

  private:
    Point b_;
  };

  double getFigureArea(const Polygon& figure);

  double odd(double acc, const Polygon& figure);
  double even(double acc, const Polygon& figure);

  bool odd(const Polygon& figure);
  bool even(const Polygon& figure);

  double getArea(
    const std::vector<Polygon>& figures,
    double (*functor)(double, const Polygon&) =
    [](double acc, const Polygon& figure) { return acc + getFigureArea(figure); });

  double getAreaMean(const std::vector< Polygon >& figures);

  double getAreaOfPoints(const std::vector< Polygon >& figures, int number);

  bool areaComparator(const Polygon& figure1, const Polygon& figure2);
  bool vertexComparator(const Polygon& figure1, const Polygon& figure2);

  double maxArea(const std::vector< Polygon >& figures);
  int maxVertex(const std::vector< Polygon >& figures);

  double minArea(const std::vector< Polygon >& figures);
  int minVertex(const std::vector< Polygon >& figures);

  int count(const std::vector< Polygon >& figures, bool (*functor)(const Polygon&));
  int count(const std::vector< Polygon >& figures, int numberOfVertex);

  bool xPointComparator(const Point& point1, const Point& point2);
  bool yPointComparator(const Point& point1, const Point& point2);

  bool xComparator(const Polygon& figure1, const Polygon& figure2);

  bool yComparator(const Polygon& figure1, const Polygon& figure2);

  bool inFrame(const Polygon& figure, const std::vector< Polygon >& figures);

  class EqualFunctor
  {
  public:

    EqualFunctor(const Point& point1, const Point& point2) :
      coordinate_({ (point2.x - point1.x),  (point2.y - point1.y) })
    {}

    bool operator() (const Point& point1, const Point& point2);

  private:

    std::pair<int, int> coordinate_;

  };

  bool fullPointComparator(const Point& point1, const Point& point2);

  bool isSame(const Polygon& figure1, const Polygon& figure2);

  int same(const Polygon& figure, const std::vector< Polygon >& figures);
}

#endif
