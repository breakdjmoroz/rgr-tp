#include <iostream>
#include <iomanip>
#include <functional>
#include <string>
#include <Iofmtguard.h>

#include "BaseTypes.h"
#include "CommandsOutput.h"

namespace moroz
{
  void printArea(const std::vector< Polygon >& figures, std::istream& bufferIn)
  {
    std::string parameter = "";

    int numberOfVertex = 0;

    if (bufferIn >> numberOfVertex && ((bufferIn.peek() == '\n') || bufferIn.eof()))
    {
      if (numberOfVertex > 2)
      {
        Iofmtguard guardian(std::cout);
        std::fixed(std::cout);
        std::cout << std::setprecision(1) << getAreaOfPoints(figures, numberOfVertex) << '\n';
      }
      else
      {
        std::cout << INVALID_COMMAND << '\n';
      }
    }
    else
    {
      bufferIn.clear();

      bufferIn >> parameter;

      if (bufferIn && ((bufferIn.peek() == '\n') || bufferIn.eof()))
      {
        if (parameter == "ODD")
        {
          Iofmtguard guardian(std::cout);
          std::fixed(std::cout);
          std::cout << std::setprecision(1) << getArea(figures, moroz::odd) << '\n';
        }
        else if (parameter == "EVEN")
        {
          Iofmtguard guardian(std::cout);
          std::fixed(std::cout);
          std::cout << std::setprecision(1) << getArea(figures, moroz::even) << '\n';
        }
        else if (parameter == "MEAN")
        {
          if (figures.size())
          {
            Iofmtguard guardian(std::cout);
            std::fixed(std::cout);
            std::cout << std::setprecision(1) << getAreaMean(figures) << '\n';
          }
          else
          {
            std::cout << INVALID_COMMAND << '\n';
          }
        }
        else
        {
          std::cout << INVALID_COMMAND << '\n';
        }
      }
      else
      {
        std::cout << INVALID_COMMAND << '\n';
      }
    }
  }

  void printMax(const std::vector< Polygon >& figures, std::istream& bufferIn)
  {
    std::string parameter = "";

    bufferIn >> parameter;

    if (bufferIn && ((bufferIn.peek() == '\n') || bufferIn.eof()))
    {
      if (parameter == "AREA" && figures.size() != 0)
      {
        Iofmtguard guardian(std::cout);
        std::fixed(std::cout);
        std::cout << std::setprecision(1) << maxArea(figures) << '\n';
      }
      else if (parameter == "VERTEXES" && figures.size() != 0)
      {
        Iofmtguard guardian(std::cout);
        std::cout << maxVertex(figures) << '\n';
      }
      else
      {
        std::cout << INVALID_COMMAND << '\n';
      }
    }
    else
    {
      std::cout << INVALID_COMMAND << '\n';
    }
  }

  void printMin(const std::vector< Polygon >& figures, std::istream& bufferIn)
  {
    std::string parameter = "";

    bufferIn >> parameter;

    if (bufferIn && ((bufferIn.peek() == '\n') || bufferIn.eof()))
    {
      if (parameter == "AREA" && figures.size() != 0)
      {
        Iofmtguard guardian(std::cout);
        std::fixed(std::cout);
        std::cout << std::setprecision(1) << minArea(figures) << '\n';
      }
      else if (parameter == "VERTEXES" && figures.size() != 0)
      {
        Iofmtguard guardian(std::cout);
        std::cout << minVertex(figures) << '\n';
      }
      else
      {
        std::cout << INVALID_COMMAND << '\n';
      }
    }
    else
    {
      std::cout << INVALID_COMMAND << '\n';
    }
  }

  void printCount(const std::vector< Polygon >& figures, std::istream& bufferIn)
  {
    std::string parameter = "";

    int numberOfVertex = 0;

    if (bufferIn >> numberOfVertex)
    {
      Iofmtguard guardian(std::cout);
      if (numberOfVertex > 2)
      {
        std::cout << count(figures, numberOfVertex) << '\n';
      }
      else
      {
        std::cout << INVALID_COMMAND << '\n';
      }
    }
    else
    {
      bufferIn.clear();

      bufferIn >> parameter;

      if (bufferIn && ((bufferIn.peek() == '\n') || bufferIn.eof()))
      {
        if (parameter == "ODD")
        {
          Iofmtguard guardian(std::cout);
          std::cout << count(figures, moroz::odd) << '\n';
        }
        else if (parameter == "EVEN")
        {
          Iofmtguard guardian(std::cout);
          std::cout << count(figures, moroz::even) << '\n';
        }
        else
        {
          std::cout << INVALID_COMMAND << '\n';
        }
      }
      else
      {
        std::cout << INVALID_COMMAND << '\n';
      }
    }
  }

  void printInFrame(const std::vector< Polygon >& figures, std::istream& bufferIn)
  {
    Polygon parameter;

    bufferIn >> parameter;

    if (bufferIn && ((bufferIn.peek() == '\n') || bufferIn.eof()))
    {
      Iofmtguard guardian(std::cout);
      std::cout << ((inFrame(parameter, figures)) ? "<TRUE>" : "<FALSE>") << '\n';
    }
    else
    {
      std::cout << INVALID_COMMAND << '\n';
    }
  }

  void printSame(const std::vector< Polygon >& figures, std::istream& bufferIn)
  {
    Polygon parameter;

    bufferIn >> parameter;

    if (bufferIn && ((bufferIn.peek() == '\n') || bufferIn.eof()))
    {
      Iofmtguard guardian(std::cout);
      std::cout << same(parameter, figures) << '\n';
    }
    else
    {
      std::cout << INVALID_COMMAND << '\n';
    }
  }
}

