#ifndef COMMANDS_OUTPUT
#define COMMANDS_OUTPUT

#include <iosfwd>
#include <vector>
#include <string>

#include "Commands.h"

namespace moroz
{
  const std::string INVALID_COMMAND = "<INVALID COMMAND>";

  void printArea(const std::vector< Polygon >& figures, std::istream& bufferIn);
  void printMax(const std::vector< Polygon >& figures, std::istream& bufferIn);
  void printMin(const std::vector< Polygon >& figures, std::istream& bufferIn);
  void printCount(const std::vector< Polygon >& figures, std::istream& bufferIn);
  void printInFrame(const std::vector< Polygon >& figures, std::istream& bufferIn);
  void printSame(const std::vector< Polygon >& figures, std::istream& bufferIn);
}

#endif
