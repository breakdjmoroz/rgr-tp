#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <map>
#include <limits>
#include <Iofmtguard.h>

#include "BaseTypes.h"
#include "Commands.h"
#include "CommandsOutput.h"

int main(int argc, char* argv[])
{
  if (argc == 1)
  {
    std::cerr << "Error 1: No filename to find.";
    return 1;
  }

  std::ifstream in(argv[1]);

  if (!in)
  {
    std::cerr << "Error 2: No such file.";
    in.close();
    return 2;
  }

  using moroz::Polygon;
  using moroz::Iofmtguard;

  std::vector< Polygon > figures;

  while (!in.eof())
  {
    in.clear();
    std::copy(std::istream_iterator< Polygon >(in), std::istream_iterator< Polygon >(), std::back_inserter(figures));
  }

  in.close();

  std::map< std::string, void (*)(const std::vector< Polygon >&, std::istream&) > commandMap;

  commandMap.insert({ "AREA", moroz::printArea });
  commandMap.insert({ "MAX", moroz::printMax });
  commandMap.insert({ "MIN", moroz::printMin });
  commandMap.insert({ "COUNT", moroz::printCount });
  commandMap.insert({ "INFRAME", moroz::printInFrame });
  commandMap.insert({ "SAME", moroz::printSame });

  while (!std::cin.eof())
  {
    Iofmtguard globalGuardian(std::cout);

    std::cin.clear();

    std::string command = "";

    std::cin >> command;

    auto temp = commandMap.find(command);

    if (!std::cin.eof())
    {
      if (temp != commandMap.cend())
      {
        temp->second(figures, std::cin);
      }
      else
      {
        std::cout << moroz::INVALID_COMMAND << '\n';

        std::cin.ignore(std::numeric_limits< std::streamsize >::max(), '\n');
      }
    }
  }

  return 0;
}
