#include <iostream>

#include "Iofmtguard.h"

namespace moroz
{
  Iofmtguard::Iofmtguard(std::basic_ios< char >& in) :
    string_(in),
    fill_(in.fill()),
    fmt_(in.flags())
  {}

  Iofmtguard::~Iofmtguard()
  {
    string_.fill(fill_);
    string_.flags(fmt_);
  }
}
