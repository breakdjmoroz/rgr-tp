#ifndef IOFMTGUARD
#define IOFMTGUARD

#include <ios>

namespace moroz
{
  class Iofmtguard
  {
  public:

    Iofmtguard(std::basic_ios< char >& string);
    ~Iofmtguard();

  private:

    std::basic_ios< char >& string_;
    char fill_;
    std::basic_ios< char >::fmtflags fmt_;
  };
}

#endif
