#ifndef POINT_KEYS
#define POINT_KEYS

#include <iosfwd>

namespace moroz
{
  struct DelimeterIO
  {
    char exp;
  };

  std::istream& operator>> (std::istream& in, DelimeterIO&& dest);
}

#endif
